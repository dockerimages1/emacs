;; Define where various packages I wish to use, that come in
;; form of Git checkouts, or that I've written, is kept.
(add-to-list 'load-path "~/.emacs.d/lisp/")

(require 'htmlize)
(require 'ox-twbs)
