######################################################################
#
# This is a utility Docker image that provides Emacs for exporting
# org-mode documents into HTML, and perhaps other batch operations.

FROM ubuntu:22.04

RUN apt-get -q update                                 \
 && apt-get -q upgrade --yes --no-install-recommends  \
 && apt-get -q install --yes curl emacs               \
 && apt-get -q install --yes curl openssh-client      \
 && rm -rf /var/lib/apt/lists/*

# Set up home as a non-root user, for security reasons
RUN useradd --create-home emacs
USER emacs
ENV HOME /home/emacs
WORKDIR ${HOME}

COPY --chown=emacs:emacs init.el ${HOME}/.emacs.d/init.el
COPY --chown=emacs:emacs known_hosts ${HOME}/.ssh/known_hosts

RUN mkdir -p ~/.emacs.d/lisp \
 && curl -L https://raw.githubusercontent.com/hniksic/emacs-htmlize/master/htmlize.el > ~/.emacs.d/lisp/htmlize.el \
 && curl -L https://github.com/marsmining/ox-twbs/raw/master/ox-twbs.el > ~/.emacs.d/lisp/ox-twbs.el

###
# By leaving ENTRYPOINT empty but providing CMD, we allow the default
# behavior when running the Docker container of starting up Emacs
# but also to run something else manually by invoking the container
# with /bin/bash to get a shell prompt.

ENTRYPOINT []

# Only utilized if NO command is supplied when running the container,
# this defines the "default executable" of a Docker image.
CMD ["emacs"]
